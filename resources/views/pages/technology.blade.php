@extends('layouts.app')

@section('title')
Technology
@endsection

@section ('content')

<section >
    <div class="jumbotron-technology jumbotron-fluid" >
        <div class="container">
            <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> Technology </strong></h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 d-flex align-items-center" style="margin-left: 30px">
                    <li style="margin-left: -350px;" class="breadcrumb-item"><a href="index.html" class="link"><i
                                class="mdi mdi-home-outline fs-4"></i></a></li>

                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page">Technology</li>
                </ol>
            </nav>

        </div>
</section>

<section id="roatex" style="margin-bottom:10px">
    <div class="container justify-content-center">
        <div class="row  info-panel-technology">

        </div>
    </div>
</section>

<section id="footer" style="background-color:#373737; ">
    <div class="row justify-content-center">
        <div class="col-sm-4 " style="margin-top:50px" >
            <div class="row" style="margin-left: 50px; ">
                <img style="width:193px; height:61px" src= {{URL('assets/images/roatexwhite.png')}} alt="">
                <p style="margin-top: 30px" class="text-white"> Indonesia Stock Exchange Building Tower II, 19th Floor, Suite 1903, Jl. Jendral Sudirman, Kav 52-53, Lot 2 - Jakarta 12190</p>
                <div class="col">
                    <img src= {{URL('public/assets/images/icon/facebook-fill.png')}} alt="">
                    <img style="margin-left: 10px" src= {{URL('assets/images/icon/instagram-fill.png')}} alt="">
                    <img src= {{URL('public/assets/images/icon/twitter-fill.png')}} alt="">
                    <img src= {{URL('public/assets/images/icon/skype-fill.png')}} alt="">
                </div>
            </div>
        </div>
        <div class="col-md-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold" style="margin-left:-200">Company</h4>
                <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">About Us</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Work</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Client</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Blog</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Contact Us</a>
                  </p>

            </div>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold">Services</h4>
                <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">MLFF Account</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Get a Cassual Pass</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Installing Tag</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Pay Toll</a>
                  </p>
            </div>
        </div>
        <div class="col-md-1 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:70px; margin-bottom:100px">
                <p class="text-secondary">Discover Cantas App</p>
                <img style="margin-bottom: 10px" src="assets/images/logo.png" alt="">
                <div class="col">
                    <img style="margin-top:10px" src= {{URL('public/assets/images/Googleplay.png')}} alt="">
                    <img style="margin-top:10px" src= {{URL('public/assets/images/Playstore.png')}} alt="">
                </div>
            </div>
        </div>
    </div>

</section>

<section id="copyright" style="background-color:#F1A501">
    <div class="container-copyright">
        <div class="row" >
            <div class="col-1" style="margin-top: 10px">
                <img src= {{URL('public/assets/images/icon/Group.png')}} alt="" style="width:25px; height:25px; margin-left:30px">

             </div>
             <div class="col-7" style="margin-top: 10px">
                <p style="margin-left:-50px" class="text-white" > Copyright 2021. All Right Reserved By PT. Roatex Indonesia Toll System</p>

             </div>
        </div>
    </div>
</section>
@endsection
