@extends('layouts.app')

@section('title')
    Homepage
@endsection

@section('content')

    <section id="hero" style="background-color: #FFF7F4;">
        <div class="row" >
            <div class="col-5" style="margin-left: 50px; margin-top: 120px">
                <h1 class="hero-font" style="font-weight: 600; font-size:44px; font-style:normal">
                    Multi Lane Free Flow  <br>(MLFF) Touchless Cashless <br> Toll transaction system.
                </h1>
                <p style="font-weight: 200px">
                    Sistem transaksi nontunai berbasis MLFF menjadi salah satu inovasi baru melalui sistem pembayaran nirsentuh dengan menciptakan efisiensi, efektivitas, aman, dan nyaman dalam penerapan sistem pembayaran tol di Indonesia.
                </p>
                {{-- <a href="#" class="btn btn-warning text-white">Pelajari</a> --}}
            </div>
            <div class="col-6" style="margin-top: 100px; margin-bottom: 70px;">
                <img style="width: 637px; height:473px" src= {{URL('public/assets/images/hero.png')}} class="w-100 float-right" alt="">
            </div>
        </div>
    </section>

    <section id="about-us" >
        <div class="row">
            <div class="col-5" style="margin-left: 50px; margin-top: 120px; margin-bottom: 70px; positon:absolute">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" style="width:500px; height:280px" src= {{URL('public/assets/video/cantasvideo.mp4')}}
                     frameborder="0" data-quality="Full HD" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-6" style="margin-left: 50px; margin-top: 120px; positon:absolute">
                <h5 style="color:#7B7B7B">
                    About Us
                </h5>
                <h1 class="hero-font" style="font-weight: 700; font-size:44px; font-style:normal">
                    Roatex  <span style="color: #F1A501">Committed</span> To Efficiency <br> Of Toll Transaction System
                </h1>
                <p style="font-weight: 200px">
                    This technology is able to recognize and identify the user's vehicle, so <br> that transactions can be carried out directly more quickly and <br> efficiently without queues and delays....                </p>
                <a href="{{route('aboutus-index')}}" class="btn btn-warning text-white">Pelajari Lebih Lanjut</a>
            </div>
        </div>
    </section>

    <section id="services" style="background-color: #FFF7F4;">
        <div class="container">
            <div class="row">
                <h5 style="margin-top:40px; color: #F1A501">
                    Services ___
                </h5>
                <div class="col-sm-6" style="margin-top: 30px">
                    <h1 class="hero-font" style="font-weight: 700; font-size:44px; font-style:normal">
                        We Offer a Wide Variety <br> Of Services.
                    </h1>
                </div>
                {{-- <div class="col-sm-4" style="margin-top: 40px; margin-right: -200px; margin-left:300px; positon:absolute">
                    <a href="{{route('services-index')}}" class="btn btn-warning text-white">View All</a>
                </div> --}}

                <div class="row justify-content-center">
                    <div class="col-3 index-services">
                        <div class="card" style="background-color: #FFF7F4; border:none">

                                <div class="d-flex justify-content-center" style="positon:absolute">
                                    <img src={{URL('public/assets/images/Icon/Icon1.png')}} alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                </div>
                                <p style="" class="text-black text-center" style="text-align: left; margin-left: 20px; margin-top: 10px" > <strong>Optimizing Traffic Flow</strong> </p>
                                    {{-- <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Main objective of our e-toll systems is to avoid congestion and optimize the flow of traffic ensuring a free flow of vehicles on the road.</p> --}}

                        </div>
                    </div>
                    <div class="col-3 index-services">
                        <div class="card" style="background-color: #FFF7F4; border:none">

                                <div class="d-flex justify-content-center" style="positon:absolute">
                                    <img src={{URL('public/assets/images/Icon/Icon2.png')}}  alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                </div>
                                <p class="text-black text-center" style="text-align: left; margin-left: 20px; margin-top: 10px" > <strong> Decreasing Toll Collection Cost</strong> </p>
                                {{-- <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Our aim is to design financially feasible e-toll systems, which using the right technology may not only decrease the toll collection costs, but also increase the income due to the efficiency of the system.</p> --}}

                        </div>
                    </div>
                    <div class="col-3 index-services">
                        <div class="card" style="background-color: #FFF7F4;  border:none">

                                <div class="d-flex justify-content-center" style="positon:absolute">
                                    <img src={{URL('public/assets/images/Icon/Icon3.png')}}  alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                </div>
                                <p class="text-black text-center" style="text-align:; margin-left: 20px; margin-top: 10px" > <strong>Minimizing Costs Borne By Toll Road Users</strong> </p>
                                {{-- <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">We deliver the best possible e-toll solutions to road owners, operators and users alike. It’s one of our main goals to create user friendly, convenient e-toll systems, which at the same time do not need to result in higher prices.</p> --}}
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-3 index-services">
                        <div class="card" style="background-color: #FFF7F4; border:none">

                                <div class="d-flex justify-content-center" style="positon:absolute">
                                    <img src={{URL('public/assets/images/Icon/Icon1.png')}} alt="" style="width: 80px; height: 80px;margin-left: 5px;">
                                </div>
                                <p style="" class="text-black text-center" style="text-align: left; margin-left: 20px;" > <strong>Maximizing The Usage Of Existing Infrastructures</strong> </p>
                                    {{-- <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Main objective of our e-toll systems is to avoid congestion and optimize the flow of traffic ensuring a free flow of vehicles on the road.</p> --}}

                        </div>
                    </div>
                    <div class="col-3 index-services">
                        <div class="card" style="background-color: #FFF7F4; border:none">

                                <div class="d-flex justify-content-center" style="positon:absolute">
                                    <img src={{URL('public/assets/images/Icon/Icon2.png')}}  alt="" style="width: 80px; height: 80px;margin-left: 5px;">
                                </div>
                                <p class="text-black text-center" style="text-align: left; margin-left: 20px; " > <strong>Adapting Fully To The Local Conditions And Resources</strong> </p>
                                {{-- <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Our aim is to design financially feasible e-toll systems, which using the right technology may not only decrease the toll collection costs, but also increase the income due to the efficiency of the system.</p> --}}

                        </div>
                    </div>
                    <div class="col-3 index-services">
                        <div class="card" style="background-color: #FFF7F4;  border:none">

                                <div class="d-flex justify-content-center" style="positon:absolute">
                                    <img src={{URL('public/assets/images/Icon/Icon3.png')}}  alt="" style="width: 80px; height: 80px;margin-left: 5px;">
                                </div>
                                <p class="text-black text-center" style="text-align:; margin-left: 20px; " > <strong>Integrating Several Payment Method And Partners</strong> </p>
                                {{-- <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">We deliver the best possible e-toll solutions to road owners, operators and users alike. It’s one of our main goals to create user friendly, convenient e-toll systems, which at the same time do not need to result in higher prices.</p> --}}
                        </div>
                    </div>
                </div>


                {{-- <div class="row">
                    <div class="col-lg-3">
                        <a href="#">
                            <div class="item wow bounceInUp animated" data-wow-duration="1s"
                            data-wow-delay="0.3s" style="visibility: visible;">
                            </div>
                        </a>
                    </div>
                </div> --}}

                {{-- <div class="container" id="slider">
                    <div class="slider justify-content-start ">
                        <div class="card row">
                            <div class="image justify-content-between">
                            <img src="../assets/images/Icon/Icon1.png" alt="">
                            <img src="../assets/images/Icon/Shape.png" alt="">
                        </div>
                        <div class="card-body">
                            <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >MLFF account</h5>
                            <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">You can open an account with cash, a cheque or money order in person</p>
                            <a href="#" class="text-sm-left" style="margin-left: 20px;  text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                        </div>
                        </div>
                    </div>
                    <div class="slider justify-content-start ">
                        <div class="card row">
                            <div class="image justify-content-between">
                            <img src="../assets/images/Icon/Icon1.png" alt="">
                            <img src="../assets/images/Icon/Shape.png" alt="">
                        </div>
                        <div class="card-body">
                            <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >MLFF account</h5>
                            <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">You can open an account with cash, a cheque or money order in person</p>
                            <a href="#" class="text-sm-left" style="margin-left: 20px;  text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                        </div>
                        </div>
                    </div>
                    <div class="slider justify-content-start ">
                        <div class="card row">
                            <div class="image justify-content-between">
                            <img src="../assets/images/Icon/Icon1.png" alt="">
                            <img src="../assets/images/Icon/Shape.png" alt="">
                        </div>
                        <div class="card-body">
                            <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >MLFF account</h5>
                            <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">You can open an account with cash, a cheque or money order in person</p>
                            <a href="#" class="text-sm-left" style="margin-left: 20px;  text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                        </div>
                        </div>
                    </div>
                    <div class="slider justify-content-start ">
                        <div class="card row">
                            <div class="image justify-content-between">
                            <img src="../assets/images/Icon/Icon1.png" alt="">
                            <img src="../assets/images/Icon/Shape.png" alt="">
                        </div>
                        <div class="card-body">
                            <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >MLFF account</h5>
                            <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">You can open an account with cash, a cheque or money order in person</p>
                            <a href="#" class="text-sm-left" style="margin-left: 20px;  text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                        </div>
                        </div>
                    </div>
                    <div class="slider justify-content-start ">
                        <div class="card row">
                            <div class="image justify-content-between">
                            <img src="../assets/images/Icon/Icon1.png" alt="">
                            <img src="../assets/images/Icon/Shape.png" alt="">
                        </div>
                        <div class="card-body">
                            <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >MLFF account</h5>
                            <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">You can open an account with cash, a cheque or money order in person</p>
                            <a href="#" class="text-sm-left" style="margin-left: 20px;  text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                        </div>
                        </div>
                        <i class="fas fa-chevron-right arrow"></i>
                    </div>
                </div> --}}
                {{-- <div class="d-flex justify-content-start" style="margin-top: 50px; margin-bottom:50px; positon:absolute">
                    <div class="col-sm-3 text-center me-3">
                        <div class="card" style="background-color: white">
                            <div class="row">
                                <div class="d-flex justify-content-between" style="positon:absolute">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 100px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >OPTIMIZING TRAFFIC FLOW</h5>
                                <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Main objective of our e-toll systems is to avoid congestion and optimize the flow of traffic ensuring a free flow of vehicles on the road.</p>
                                <a href="#" class="text-sm-left" style="text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center me-3">
                        <div class="card" style="background-color: white">
                            <div class="row">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 100px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >DECREASING TOLL COLLECTION COST</h5>
                                <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Our aim is to design financially feasible e-toll systems, which using the right technology may not only decrease the toll collection costs, but also increase the income due to the efficiency of the system.</p>
                                <a href="#" style="margin-left:-20px; text-decoration:none; color:#F1A501">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 text-center me-3">
                        <div class="card" style="background-color: white">
                            <div class="row">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 100px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="text-black" style="text-align: left; margin-left: 20px; margin-top: 10px" >MINIMIZING COSTS BORNE BY TOLL ROAD USERS</h5>
                                <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">We deliver the best possible e-toll solutions to road owners, operators and users alike. It’s one of our main goals to create user friendly, convenient e-toll systems, which at the same time do not need to result in higher prices.</p>
                                <a href="#" style="text-align: left; text-decoration:none; color:#F1A501">Read more</a>
                            </div>
                        </div>
                    </div>
                </div> --}}

                {{-- <div class="services-category">
                    <button class="pre-btn"><img src="../assets/images/icon/arrow.png" alt="">
                    </button>
                    <button class="nxt-btn"><img src="../assets/images/icon/arrow.png" alt="">
                    </button>
                    <div class="services-container">
                        <div class="services-card">
                            <div class="d-flex justify-content-between">
                                <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <ul id="autoWidth" class="cs-hidden">
                    <li class="item-a">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>

                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>
                    <li class="item-b">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>
                    <li class="item-c">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>
                    <li class="item-d">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>
                    <li class="item-e">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>
                    <li class="item-f">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>
                    <li class="item-a">
                        <div class="box">
                            <div class="slide-img">
                                <div class="d-flex justify-content-between">
                                    <img src="../assets/images/Icon/Icon1.png" alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                                    <img src="../assets/images/Icon/Shape.png" alt="" style="width: 80px; height: 80px;margin-left: 200px; margin-top: 10px; position:absolute">
                                </div>
                            </div>
                            <div class="detail-box">
                            </div>
                        </div>
                    </li>

                </ul> --}}

            </div>
        </div>
    </section>

    <section id="technology">
        <div class="row">
            <div class="d-flex justify-content-center" style="margin-top: 100px">
                <h5 style="color: #7B7B7B">Technology</h5>
            </div>
            <div class="d-flex justify-content-center" style="margin-top: 20px">
                <h1 class="hero-font text-center" style="font-weight: 700; font-size:44px; font-style:normal">Some <span style="color: #F1A501"> Great <br>
                    Technology</span> Implementation</h1>
            </div>
            <div class="d-flex justify-content-between" style="margin-top: 50px; margin-bottom:50px">
                <div class="col-sm-1 ">
                </div>
                <div class="col-sm-4 text-center">
                    <div class="card" style="background-color: #F1A501">
                        <img src= {{URL('public/assets/images/homepage/Profile.png')}} alt="" style="width: 80px; height: 80px;margin-left: -30px; margin-top: 30px; position:absolute">
                        <div class="card-body">
                            <h5 class="text-white" style="text-align: left; margin-left: 50px; margin-top: 30px" >GNSS (Global Navigation <br> Satellite System)</h5>
                            <p class="text-white" style="margin-left: 50px; margin-top: 20px; text-align: justify">GNSS tersebut merupakan teknologi yang digunakan untuk menentukan posisi atau lokasi (lintang, bujut, dan ketinggian) serta waktu dalam satuan ilmiah di bumi.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center margin-bottom: 30px">
                    <div class="card" style="background-color: #F1A501">
                        <img src={{URL('public/assets/images/homepage/Image.png')}} alt="" style="width: 80px; height: 80px;margin-left: -30px; margin-top: 30px; position:absolute">
                        <div class="card-body">
                            <h5 class="text-white" style="text-align: left; margin-left: 50px; margin-top: 30px">Mobile Tolling</h5>
                            <p class="text-white" style="margin-left: 50px; margin-top: 20px; text-align: justify">GNSS tersebut merupakan teknologi yang digunakan untuk menentukan posisi atau  lokasi (lintang, bujut, dan ketinggian) serta waktu dalam satuan ilmiah di bumi.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                </div>
            </div>
        </div>
    </section>

    <section id="news" style="background-color: #FFF7F4; positon:absolute">
        <div class="container-news">
            <div class="row">
                <div class="d-flex justify-content-center" style="margin-top: 100px; positon:absolute">
                    <h5 style="color: #F1A501">__ News __</h5>
                </div>
                <div class="d-flex justify-content-center" style="margin-top: 20px; positon:absolute">
                    <h1 class="hero-font text-center" style="font-weight: 700; font-size:44px; font-style:normal"> Every Singel Update From Us </h1>
                </div>
                <div class="d-flex justify-content-start" style="margin-top: 50px; margin-bottom:50px; positon:absolute">
                    <div class="col-sm-4 text-left me-3">
                        <div class="card" style="background-color: white">
                            <div class="row">
                                <img src={{URL('public/assets/images/homepage/news1.png')}}  alt="" style="margin-left: -7px; margin-top:10px">
                            </div>
                            <div class="card-body-indexnews">
                                <h5 class="text-black" style="text-align: left; margin-left: 0px; margin-top: 10px" >Kementerian PUPR Tunjuk Roatex Ltd. Jadi Pelaksana Transaksi Tol Tanpa Sentuh</h5>
                                <div class="col-sm-10 justify-content-center" style="margin-left:5px">
                                    {{-- <div class="d-flex" style="margin-left: 5px" >
                                        <img class="img-right" style="margin-top:20px; width:14px; height:14px;" src= {{URL('assets/images/Icon/time.png')}}  alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>January 25, 2021</small></p>
                                        <img class="img-right"  style="margin-top:20px; margin-left:auto; width:14px; height:14px" src= {{URL('assets/images/Icon/person.png')}} alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>Cristofer Westervelt</small></p>
                                        <img class="img-right"  style="margin-top:20px; margin-left:auto; width:14px; height:14px" src= {{URL('assets/images/Icon/comment.png')}} alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>10 Comment</small></p>
                                    </div> --}}
                                </div>
                                <p class="text-black" style="margin-left: 0px; margin-top: 20px; text-align: justify">
                                    Bisnis.com, JAKARTA – Kementerian Pekerjaan Umum dan Perumahan Rakyat (PUPR) terus meningkatkan tata kelola sistem layanan tol melalui penerapan sistem transaksi Tol Nontunai Nirsentuh Multi Lane Free Flow (MLFF).....
                                </p>
                                <a style="margin-top: 20px; "
                                href="https://ekonomi.bisnis.com/read/20220127/45/1494067/kementerian-pupr-tunjuk-roatex-ltd-jadi-pelaksana-transaksi-tol-tanpa-sentuh"
                                class="btn btn-warning text-white">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 text-left me-3">
                        <div class="card" style="background-color: white">
                            <div class="row">
                                <img src={{URL('public/assets/images/homepage/news2.png')}}  alt="" style="margin-left: -7px; margin-top:10px">
                            </div>
                            <div class="card-body">
                                <h5 class="text-black" style="text-align: left; margin-left: 0px; margin-top: 10px" >
                                    Roatex Indonesia akan Sediakan Aplikasi Cantas untuk Mengimplementasikan Sistem MLFF
                                </h5>
                                <div class="col-sm-10 justify-content-center" style="margin-left:5px">
                                    {{-- <div class="d-flex" style="margin-left: 5px" >
                                        <img class="img-right" style="margin-top:20px; width:14px; height:14px;" src={{URL('assets/images/Icon/time.png')}} alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>January 25, 2021</small></p>
                                        <img class="img-right"  style="margin-top:20px; margin-left:auto; width:14px; height:14px" src= {{URL('assets/images/Icon/person.png')}}  alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>Cristofer Westervelt</small></p>
                                        <img class="img-right"  style="margin-top:20px; margin-left:auto; width:14px; height:14px" src= {{URL('assets/images/Icon/comment.png')}} alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>10 Comment</small></p>
                                    </div> --}}
                                </div>
                                <p class="text-black" style="margin-left: 0px; margin-top: 20px; text-align: justify">
                                    KONTAN.CO.ID - JAKARTA. PT Roatex Indonesia Toll System (RITS) menyatakan pihaknya sedang mengembangkan sistem pembayaran tol non tunai nirsentuh atau Multi Lane Free Flow (MLFF) bernama Cantas melalui aplikasi dan berencana menggandeng pihak operator telekomunikasi.....                    </p>
                                </p>
                                <a style="margin-top: 20px; "
                                href="https://industri.kontan.co.id/news/roatex-indonesia-akan-sediakan-aplikasi-cantas-untuk-mengimplementasikan-sistem-mlff"
                                class="btn btn-warning text-white">Read More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 text-left me-3">
                        <div class="card" style="background-color: white">
                            <div class="row">
                                <img src= {{URL('public/assets/images/homepage/news3.png')}}  alt="" style="margin-left: -7px; margin-top:10px">
                            </div>
                            <div class="card-body">
                                <h5 class="text-black" style="text-align: left; margin-left: 0px; margin-top: 10px" >
                                    PT Roatex Indonesia Toll System Tangani Teknologi MLFF Bersama BPJT
                                </h5>
                                <div class="col-sm-10 justify-content-center" style="margin-left:5px">
                                    {{-- <div class="d-flex" style="margin-left: 5px" >
                                        <img class="img-right" style="margin-top:20px; width:14px; height:14px;" src= {{URL('assets/images/Icon/time.png')}}  alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>January 25, 2021</small></p>
                                        <img class="img-right"  style="margin-top:20px; margin-left:auto; width:14px; height:14px" src= {{URL('assets/images/Icon/person.png')}}  alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>Cristofer Westervelt</small></p>
                                        <img class="img-right"  style="margin-top:20px; margin-left:auto; width:14px; height:14px" src= {{URL('assets/images/Icon/comment.png')}}  alt="">
                                        <p class="text-center" style="margin-top:15px; font-size: 15px"><small>10 Comment</small></p>
                                    </div> --}}
                                </div>
                                <p class="text-black" style="margin-left: 0px; margin-top: 20px; text-align: justify">
                                    INDOWORK.ID, JAKARTA: BPJT Kementerian PUPR dengan PT Roatex Indonesia Toll System (RITS) menandatangani kerjasama Sistem Transaksi Tol Nontunai Nirsentuh Berbasis Multi Lane Free Flow (MLFF). Penandatanganan dilaksanakan langsung oleh Kepala BPJT Danang Parikesit dengan Direktur Utama PT Roatex Indonesia Toll System (RITS), Peter Ong di Kantor BPJT Kementerian PUPR di Jakarta...                    </p>
                                <a style="margin-top: 20px; "
                                href="https://indowork.id/headline-2/pt-roatex-indonesia-toll-system-tangani-teknologi-mlff-bersama-bpjt/"
                                class="btn btn-warning text-white">Read More</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

    </section>

    <section id="cantas">
        <div class="container" style="margin-top:50px; width: 1176px; height:307px; positon:absolute ">
            <div class="card-cantas" style="background-color: #F1F8FD;">
                <div class="row">
                    <div class="col-5">
                        <img style="margin-left:-17px; width: 412px; height:304px; positon:absolute" src= {{URL('public/assets/images/cantasimg.png')}} alt="">
                    </div>
                    <div class="col-7">
                        <img style="width: 217px; height:64px; margin-top: 15px; positon:absolute" src= {{URL('public/assets/images/canstasclear.png')}} alt="">
                            <p style="margin-top: 20px;  positon:absolute">Smartphones are skyrocketing as the device of choice for almost every type of transaction. Tolling is no exception, especially among Millennials. To match this wave of the future, Emovis has engineered a smartphone-based tolling application allowing toll-payment without a tag.</p>
                            <div class="col" >
                                <a style="margin-top: 20px;  positon:absolute" href="#" class="btn btn-warning text-white">Visit Site</a>
                                <img style="margin-top: 20px; margin-left:70px" src= {{URL('public/assets/images/Playstore.png')}}  alt="">
                                <img style="margin-top: 20px; margin-left:5px" src= {{URL('public/assets/images/Googleplay.png')}} alt="">
                            </div>

                    </div>

                </div>
            </div>

        </div>

    </section>

    <section id="subscribe">
        <div class="container" style="margin-top:50px; width: 1176px; height:307px;  margin-bottom:50px  " >
            <div class="card" style="background-color: #F1A501; width:1170px; height:200px; margin-top:100px; margin-:bottom70px; ">
                <div class="row justify-content-center" style="margin-top: 50px">
                    <p class="text-white">Subscribe Newsletters  ______</p>
                    <div class="d-flex">
                        <div class="col-6">
                            <h4 class="text-white"><strong> Let's Stay in Touch </strong></h4>
                        </div>
                       <div class="col-5">
                        <div class="input-group mb-3">
                            <input type="email"
                            style="width:300px; height:60px"
                            class="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter your email"
                             aria-label="Recipient's username" >
                            <button style="height: 60px" class="btn btn-outline-light text-white" type="button" id="button-addon2">Subscribe Now</button>
                          </div>
                        {{-- <div class="form-group">
                            <input type="email"
                            style="margin-left:200px; width:642px; height:60px"
                            class="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter your email">

                            <a style="margin-right:-1000px; margin-top:5px;" href="#" class="btn btn-warning text-white">Subscribe Now</a>


                        </div> --}}

                       </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="footer" style="background-color:#373737; ">
        <div class="row justify-content-center">
            <div class="col-sm-4 " style="margin-top:50px" >
                <div class="row" style="margin-left: 50px; ">
                    <img style="width:193px; height:61px" src= {{URL('assets/images/roatexwhite.png')}} alt="">
                    <p style="margin-top: 30px" class="text-white"> Indonesia Stock Exchange Building Tower II, 19th Floor, Suite 1903, Jl. Jendral Sudirman, Kav 52-53, Lot 2 - Jakarta 12190</p>
                    <div class="col">
                        <img src= {{URL('public/assets/images/icon/facebook-fill.png')}} alt="">
                        <img style="margin-left: 10px" src= {{URL('assets/images/icon/instagram-fill.png')}} alt="">
                        <img src= {{URL('public/assets/images/icon/twitter-fill.png')}} alt="">
                        <img src= {{URL('public/assets/images/icon/skype-fill.png')}} alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-2 mx-auto mt-3" >
                <div class="row" style="margin-left: 30px; margin-top:50px">
                    <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold" style="margin-left:-200">Company</h4>
                    <p>
                        <a href="#" class="text-white fw-lighter" style="text-decoration: none;">About Us</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Work</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Client</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Blog</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Contact Us</a>
                      </p>

                </div>
            </div>
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3" >
                <div class="row" style="margin-left: 30px; margin-top:50px">
                    <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold">Services</h4>
                    <p>
                        <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">MLFF Account</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Get a Cassual Pass</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Installing Tag</a>
                      </p>
                      <p>
                        <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Pay Toll</a>
                      </p>
                </div>
            </div>
            <div class="col-md-1 col-lg-2 col-xl-2 mx-auto mt-3" >
                <div class="row" style="margin-left: 30px; margin-top:70px; margin-bottom:100px">
                    <p class="text-secondary">Discover Cantas App</p>
                    <img style="margin-bottom: 10px" src="assets/images/logo.png" alt="">
                    <div class="col">
                        <img style="margin-top:10px" src= {{URL('public/assets/images/Googleplay.png')}} alt="">
                        <img style="margin-top:10px" src= {{URL('public/assets/images/Playstore.png')}} alt="">
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section id="copyright" style="background-color:#F1A501">
        <div class="container-copyright">
            <div class="row" >
                <div class="col-1" style="margin-top: 10px">
                    <img src= {{URL('public/assets/images/icon/Group.png')}} alt="" style="width:25px; height:25px; margin-left:30px">

                 </div>
                 <div class="col-7" style="margin-top: 10px">
                    <p style="margin-left:-50px" class="text-white" > Copyright 2021. All Right Reserved By PT. Roatex Indonesia Toll System</p>

                 </div>
            </div>
        </div>
    </section>

    {{-- <script type="text/javascript">
        const arrows = document.querySelectorAll(".arrow");
        const container= document.querySelectorAll("#slider");

        arrows.forEach((arrow, i) => {
            const ItemNo = container[i].querySelectorAll("img").length;
            let clickitem = 0;
            arrow.addEventListener("click", () => {
                clickitem++;
                if(ItemNo - (5 + clickitem) >= 0){
                    container[i].style.transform = `translateX(${
                        container[i].computedStyleMap().get("transform")[0].x.value
                        - 455}px)`;
                }else{
                    container[i].style.transform = "translateX(0)";
                    clickitem = 0;
                }
            });
        });
    </script> --}}

@endsection
