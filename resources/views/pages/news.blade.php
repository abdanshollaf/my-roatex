@extends('layouts.app')

@section('title')
News
@endsection

@section ('content')

<section >
    <div class="jumbotron-news jumbotron-fluid">
        <div class="container">
            <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> News </strong></h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 d-flex align-items-center" style="margin-left: 30px">
                    <li style="margin-left: -200px;" class="breadcrumb-item"><a href="index.html" class="link"><i
                                class="mdi mdi-home-outline fs-4"></i></a></li>

                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page">News</li>
                </ol>
            </nav>

        </div>
</section>

<section>
    <div class="container" >
        <div class="col-lg-8">
            <img class="rounded mx-auto d-block" style="margin-top: 30px;width: 570px; height: 300px;" src={{URL('public/assets/images/news/image1.png')}}  alt="">
            <div class="card" style="margin-top: 10px; width: 570px; margin-left:80px; border:none">
                    <h3 class="hero-font" style="font-size: 25px; font-weight: 600;">
                        Kementerian PUPR Tunjuk Roatex Ltd. Jadi Pelaksana Transaksi Tol Tanpa Sentuh

                    </h3>
                    <p>
                        Bisnis.com, JAKARTA – Kementerian Pekerjaan Umum dan Perumahan Rakyat (PUPR) terus meningkatkan tata kelola sistem layanan tol melalui penerapan sistem transaksi Tol Nontunai Nirsentuh Multi Lane Free Flow (MLFF).....
                   </p>
                    <a href="https://ekonomi.bisnis.com/read/20220127/45/1494067/kementerian-pupr-tunjuk-roatex-ltd-jadi-pelaksana-transaksi-tol-tanpa-sentuh" class="btn btn-warning text-white" style="width:150px; height:45px"> <span style="font-size:20px">Baca</span> </a>

           </div>

            <img class="rounded mx-auto d-block" style="margin-top: 30px;width: 570px; height: 300px;" src={{URL('public/assets/images/news/image2.png')}}  alt="">
            <div class="card" style="margin-top: 10px; width: 570px; margin-left:80px; border:none">
                    <h3 class="hero-font" style="font-size: 25px; font-weight: 600; margin-top: 10px">
                        Roatex Indonesia akan Sediakan Aplikasi Cantas untuk Mengimplementasikan Sistem MLFF
                    </h3>
                    <p>
                        KONTAN.CO.ID - JAKARTA. PT Roatex Indonesia Toll System (RITS) menyatakan pihaknya sedang mengembangkan sistem pembayaran tol non tunai nirsentuh atau Multi Lane Free Flow (MLFF) bernama Cantas melalui aplikasi dan berencana menggandeng pihak operator telekomunikasi.....                    </p>
                    <a href="https://industri.kontan.co.id/news/roatex-indonesia-akan-sediakan-aplikasi-cantas-untuk-mengimplementasikan-sistem-mlff" class="btn btn-warning text-white" style="width:150px; height:45px"> <span style="font-size:20px">Baca</span> </a>

           </div>
            <img class="rounded mx-auto d-block" style="margin-top: 30px;width: 570px; height: 300px;" src={{URL('public/assets/images/news/image3.png')}}  alt="">
            <div class="card" style="margin-top: 10px; width: 570px; margin-left:80px; border:none">
                    <h3 class="hero-font" style="font-size: 25px; font-weight: 600; margin-top: 10px">
                        PT Roatex Indonesia Toll System Tangani Teknologi MLFF Bersama BPJT
                    </h3>
                    <p>
                        INDOWORK.ID, JAKARTA: BPJT Kementerian PUPR dengan PT Roatex Indonesia Toll System (RITS) menandatangani kerjasama Sistem Transaksi Tol Nontunai Nirsentuh Berbasis Multi Lane Free Flow (MLFF). Penandatanganan dilaksanakan langsung oleh Kepala BPJT Danang Parikesit dengan Direktur Utama PT Roatex Indonesia Toll System (RITS), Peter Ong di Kantor BPJT Kementerian PUPR di Jakarta...                    </p>
                    <a href="https://indowork.id/headline-2/pt-roatex-indonesia-toll-system-tangani-teknologi-mlff-bersama-bpjt/" class="btn btn-warning text-white" style="width:150px; height:45px"> <span style="font-size:20px">Baca</span> </a>

           </div>

        </div>
        <aside class="col-lg-4" style="margin-top: 30px">
            <section id="search">
                <nav class="navbar navbar-light bg-light">
                    <div class="container-fluid">
                      <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                      </form>
                    </div>
                </nav>
            </section>

            <section id="list-latest-post">
                <div class="list-group" style="margin-top:30px">
                    <h3>Latest Post</h3>
                    <a href="#" class="list-group-item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src={{URL('public/assets/images/Icon/Icon1.png')}} alt="">
                            </div>
                            <div class="col-lg-8" style="margin-top:20px">
                                <h4>lore ipsum</h4>
                                <p>lore ipsum blablablablablablab</p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="list-group-item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src={{URL('public/assets/images/Icon/Icon1.png')}} alt="">
                            </div>
                            <div class="col-lg-8" style="margin-top:20px">
                                <h4>lore ipsum</h4>
                                <p>lore ipsum blablablablablablab</p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="list-group-item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src={{URL('public/assets/images/Icon/Icon1.png')}} alt="">
                            </div>
                            <div class="col-lg-8" style="margin-top:20px">
                                <h4>lore ipsum</h4>
                                <p>lore ipsum blablablablablablab</p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="list-group-item">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src={{URL('public/assets/images/Icon/Icon1.png')}} alt="">
                            </div>
                            <div class="col-lg-8" style="margin-top:20px">
                                <h4>lore ipsum</h4>
                                <p>lore ipsum blablablablablablab</p>
                            </div>
                        </div>
                    </a>


                </div>
            </section>

        </aside>


    </div>


</section>

<section>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>
</section>

<section id="footer" style="background-color:#373737; ">
    <div class="row justify-content-center">
        <div class="col-sm-4 " style="margin-top:50px" >
            <div class="row" style="margin-left: 50px; ">
                <img style="width:193px; height:61px" src="../assets/images/roatexwhite.png" alt="">
                <p style="margin-top: 30px" class="text-white"> Indonesia Stock Exchange Building Tower II, 19th Floor, Suite 1903, Jl. Jendral Sudirman, Kav 52-53, Lot 2 - Jakarta 12190</p>
                <div class="col">
                    <img src={{URL('public/assets/images/icon/facebook-fill.png')}} alt="">
                    <img style="margin-left: 10px" src={{URL('public/assets/images/icon/instagram-fill.png')}} alt="">
                    <img src={{URL('public/assets/images/icon/twitter-fill.png')}} alt="">
                    <img src={{URL('public/assets/images/icon/skype-fill.png')}}  alt="">
                </div>
            </div>
        </div>
        <div class="col-md-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold" style="margin-left:-200">Company</h4>
                <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">About Us</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Work</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Client</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Blog</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Contact Us</a>
                  </p>

            </div>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold">Services</h4>
                <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">MLFF Account</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Get a Cassual Pass</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Installing Tag</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Pay Toll</a>
                  </p>
            </div>
        </div>
        <div class="col-md-1 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:70px; margin-bottom:100px">
                <p class="text-secondary">Discover Cantas App</p>
                <img style="margin-bottom: 10px" src="../assets/images/logo.png" alt="">
                <div class="col">
                    <img style="margin-top:10px" src={{URL('public/assets/images/Googleplay.png')}} alt="">
                    <img style="margin-top:10px" src={{URL('public/assets/images/Playstore.png')}} alt="">
                </div>
            </div>
        </div>
    </div>

</section>

<section id="copyright" style="background-color:#F1A501">
    <div class="container-copyright">
        <div class="row" >
            <div class="col-1" style="margin-top: 10px">
                <img src={{URL('public/assets/images/icon/Group.png')}} alt="" style="width:25px; height:25px; margin-left:30px">

             </div>
             <div class="col-7" style="margin-top: 10px">
                <p style="margin-left:-50px" class="text-white" > Copyright 2021. All Right Reserved By PT. Roatex Indonesia Toll System</p>

             </div>
        </div>
    </div>
</section>
@endsection
