@extends('layouts.app')

@section('title')
Career
@endsection

@section ('content')

<section >
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> Career</strong></h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 d-flex align-items-center" style="margin-left: 30px">
                    <li style="margin-left: -300px;" class="breadcrumb-item"><a href="index.html" class="link"><i
                                class="mdi mdi-home-outline fs-4"></i></a></li>

                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page">About Us</li>
                </ol>
            </nav>

        </div>
</section>
@endsection
