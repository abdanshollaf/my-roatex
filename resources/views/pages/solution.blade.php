@extends('layouts.app')

@section('title')
Solution
@endsection

@section ('content')
<section >
    <div class="jumbotron-solution jumbotron-fluid">

        <div class="container">
            <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> Solution </strong></h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 d-flex align-items-center" style="margin-left: 30px">
                    <li style="margin-left: -250px;" class="breadcrumb-item"><a href="index.html" class="link"><i
                                class="mdi mdi-home-outline fs-4"></i></a></li>

                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page">Solution</li>
                </ol>
            </nav>

        </div>
</section>

<section id="roatex" style="margin-bottom:50px">
    <div class="container justify-content-center">

            <div class="col-lg-10 info-panel-solution justify-content-center">
                <div style="" class="embed-responsive embed-responsive-16by9 justify-content-center">
                    <iframe class="embed-responsive-item" style="width:900px; height:510px" src="../assets/video/cantasvideo.mp4"
                        frameborder="0" data-quality="Full HD" allowfullscreen></iframe>
                </div>
            </div>

    </div>
</section>

<section>
    <div class="container"  style="margin-top:10px; width: 1176px; height:307px;">
        <div class="card" style="background-color:rgba(241, 165, 1, 0.05); width:1176px; height:200px; margin-:bottom70px; ">
            <div class="row ">
                <h2 style="font-size: 39px;font-style: normal;font-weight: 600; margin-top: 10px">Multi Lane Free Flow (MLFF)</h2>

            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-3">
                    <p class="text-center">
                        Growing populations and increasing numbers of commuters require versatile tolling solutions
                    </p>
                </div>
                <div class="col-7">
                    <p>With populations growing, commuter numbers are increasing all the time. We’re experts in keeping these increasing numbers of commuters moving freely. To do this, we can provide fast, versatile free flow tolling solutions across a number of areas.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container" style="margin-top:50px;  margin-bottom:50px">

        <div class="row">
                <h3 class="text-right" style="font-weight: 500;font-size: 25px;font-style: normal;" >
                    Definition of Multi Lane Free Flow (MLFF)
                </h3>
                <div class="col-6" style="margin-left:-80px">
                    <p class="text-right">The Multi Lane Free Flow (MLFF) Toll Transaction System is a system used to pay toll rates without the need for motorists to stop at toll road substations so that they can continue to drive. This is intended to reduce congestion due to the number of motorists who stop
                    </p>
                </div>
                <div class="col-4">
                    <div class="card" style="background-color: #F1A501; width: 351px; height:191px;margin-left:80px; margin-top:-40px">
                        <div class="col">
                            <div class="container" style="width: 360px; height:191px; margin-left:-10px;  margin-top:5px">
                                <img src="../assets/images/solution/image1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
        </div>




    </div>
</section>

<section>
    <div class="container" style="margin-top:10px; width: 1176px; height:307px;  margin-bottom: 200px;">
       <div class="card border-light" style=" width:1176px; height:200px; margin-top:100px; margin-:bottom70px; ">
        <div class="row">
            <h3 style="margin-right: -750px; font-weight: 500;font-size: 25px;font-style: normal;">
                How do I make a Multi Lane Free Flow (MLFF) payment?
            </h3>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="card" style="background-color: #F1A501; width: 351px; height:191px;margin-left:-150px; margin-top:-40px">
                    <div class="col">
                        <div class="container" style="width: 360px; height:191px; margin-left:-10px;  margin-top:5px">
                            <img src="../assets/images/solution/image2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6" style="margin-left:-100px">
                <p>Multi Lane Free Flow (MLFF) payments will use an application called Cantas. The Cantas application is planned to be free of Internet quota as an Electronic On-Board Unit (e-OBU). Users only need to download the application, and in the application there is already a built-in OBU so there is no need to buy OBU in physical form.</p>
            </div>


        </div>
       </div>
    </div>
</section>

<section id="cantas">
    <div class="container" style="margin-top:50px; width: 1176px; height:307px; positon:absolute ">
        <div class="card-cantas" style="background-color: #F1F8FD;">
            <div class="row">
                <div class="col-5">
                    <img style="margin-left:-17px; width: 412px; height:304px; positon:absolute" src= {{URL('public/assets/images/cantasimg.png')}} alt="">
                </div>
                <div class="col-7">
                    <img style="width: 217px; height:64px; margin-top: 15px; positon:absolute" src= {{URL('public/assets/images/canstasclear.png')}} alt="">
                        <p style="margin-top: 20px;  positon:absolute">Smartphones are skyrocketing as the device of choice for almost every type of transaction. Tolling is no exception, especially among Millennials. To match this wave of the future, Emovis has engineered a smartphone-based tolling application allowing toll-payment without a tag.</p>
                        <div class="col" >
                            <a style="margin-top: 20px;  positon:absolute" href="#" class="btn btn-warning text-white">Visit Site</a>
                            <img style="margin-top: 20px; margin-left:70px" src= {{URL('public/assets/images/Playstore.png')}}  alt="">
                            <img style="margin-top: 20px; margin-left:5px" src= {{URL('public/assets/images/Googleplay.png')}} alt="">
                        </div>

                </div>

            </div>
        </div>

    </div>

</section>

<section id="footer" style="background-color:#373737; ">
    <div class="row justify-content-center">
        <div class="col-sm-4 " style="margin-top:50px" >
            <div class="row" style="margin-left: 50px; ">
                <img style="width:193px; height:61px" src= {{URL('assets/images/roatexwhite.png')}} alt="">
                <p style="margin-top: 30px" class="text-white"> Indonesia Stock Exchange Building Tower II, 19th Floor, Suite 1903, Jl. Jendral Sudirman, Kav 52-53, Lot 2 - Jakarta 12190</p>
                <div class="col">
                    <img src= {{URL('public/assets/images/icon/facebook-fill.png')}} alt="">
                    <img style="margin-left: 10px" src= {{URL('assets/images/icon/instagram-fill.png')}} alt="">
                    <img src= {{URL('public/assets/images/icon/twitter-fill.png')}} alt="">
                    <img src= {{URL('public/assets/images/icon/skype-fill.png')}} alt="">
                </div>
            </div>
        </div>
        <div class="col-md-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold" style="margin-left:-200">Company</h4>
                <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">About Us</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Work</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Client</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Blog</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Contact Us</a>
                  </p>

            </div>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold">Services</h4>
                <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">MLFF Account</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Get a Cassual Pass</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Installing Tag</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Pay Toll</a>
                  </p>
            </div>
        </div>
        <div class="col-md-1 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:70px; margin-bottom:100px">
                <p class="text-secondary">Discover Cantas App</p>
                <img style="margin-bottom: 10px" src="assets/images/logo.png" alt="">
                <div class="col">
                    <img style="margin-top:10px" src= {{URL('public/assets/images/Googleplay.png')}} alt="">
                    <img style="margin-top:10px" src= {{URL('public/assets/images/Playstore.png')}} alt="">
                </div>
            </div>
        </div>
    </div>

</section>

<section id="copyright" style="background-color:#F1A501">
    <div class="container-copyright">
        <div class="row" >
            <div class="col-1" style="margin-top: 10px">
                <img src= {{URL('public/assets/images/icon/Group.png')}} alt="" style="width:25px; height:25px; margin-left:30px">

             </div>
             <div class="col-7" style="margin-top: 10px">
                <p style="margin-left:-50px" class="text-white" > Copyright 2021. All Right Reserved By PT. Roatex Indonesia Toll System</p>

             </div>
        </div>
    </div>
</section>


@endsection
