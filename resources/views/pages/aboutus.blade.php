@extends('layouts.app')

@section('title')
AboutUs
@endsection

@section ('content')

<section >
    <div class="jumbotron-aboutus jumbotron-fluid" >
        <div class="container">
            <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> About Us </strong></h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 d-flex align-items-center" style="margin-left: 30px">
                    <li style="margin-left: -300px;" class="breadcrumb-item"><a href="index.html" class="link"><i
                                class="mdi mdi-home-outline fs-4"></i></a></li>

                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                    <li class="breadcrumb-item active" aria-current="page">About Us</li>
                </ol>
            </nav>

        </div>
</section>

<!-- Info Panel--->
<section id="roatex" style="margin-bottom:50px">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 info-panel-aboutus">
                <div class="row">
                    <div class="col-lg">
                        <div style="margin-top:50px" class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" style="width:500px; height:280px" src="../assets/video/cantasvideo.mp4"
                                frameborder="0" data-quality="Full HD" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-lg" style="margin-left: 30px; positon:absolute">

                        <h1 class="hero-font" style="font-weight: 700; font-size:44px; font-style:normal">
                            Roatex <span style="color: #F1A501"> Berkomitmen
                                </span>
                                Dalam
                                <span style="color: #F1A501"> Efisiensi
                                </span>  Sistem Transaksi Toll
                        </h1>

                        <p style="font-weight: 200px">
                            Teknologi ini mampu mengenali dan mengidentifikasi kendaraan pengguna, sehingga transasksi dapat dilakukan secara langsung lebih cepat dan efisien tanpa antrean dan penundaan transaksaksi. </p>
                        <p style="font-weight: 200px">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nascetur imperdiet at a augue ullamcorper. Leo faucibus feugiat rutrum aenean. Nisi, lectus aliquet aliquam consectetur augue praesent. Lorem vulputate orci eget mi, sed pulvinar.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Akhir Info Panel--->

{{-- <section id="blankwhite">
<div class="card" style="width: 1314px; height:508px; marin-top=-300px;">
    <div class="row">
        <div class="col-4" style="margin-left: 30px; margin-top: 120px; margin-bottom: 30px; positon:absolute">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" style="width:500px; height:280px" src="../assets/video/cantasvideo.mp4"
                    frameborder="0" data-quality="Full HD" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-7" style="margin-left: 80px; margin-top: 120px; positon:absolute">

            <h1>
                Roatex <span style="color: #F1A501"> Berkomitmen
                    </span>
                    Dalam <br>
                    <span style="color: #F1A501"> Efisiensi
                    </span>  Sistem Transaksi Toll
            </h1>

            <p style="font-weight: 200px">
                Teknologi ini mampu mengenali dan mengidentifikasi kendaraan <br> pengguna, sehingga transasksi dapat dilakukan secara langsung lebih <br> cepat dan efisien tanpa antrean dan penundaan transaksaksi.
                <br>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nascetur <br> imperdiet at a augue ullamcorper. Leo faucibus feugiat rutrum aenean. <br> Nisi, lectus aliquet aliquam consectetur augue praesent. <br> Lorem vulputate orci eget mi, sed pulvinar. </p>
            <a href="#" class="btn btn-warning text-white">Pelajari Lebih Lanjut</a>
        </div>
    </div>
</div>
</section> --}}

<!-- Our Experioence--->
<section id="ourexperience" style="background-color: #FFF7F4;">
    <div class="container">
        <div class="row">
            <div class="col-5" style="margin-left: 50px; margin-top: 120px; positon:absolute">
                <h5 style="color:#7B7B7B">
                    Our Experience
                </h5>
                <h1 class="hero-font" style="font-weight: 700; font-size:44px; font-style:normal">
                    We Have Completed <span  style="color: #F1A501"> 150+ Projects
                        </span>  Succesfully

                </h1>
                <p style="font-weight: 200px">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nascetur imperdiet at a augue ullamcorper. Leo faucibus feugiat rutrum aenean. Nisi, lectus aliquet aliquam consectetur augue praesent. <br> Lorem vulputate orci eget mi, sed pulvinar.</p>
                <p> Ornare etiam erat volutpat tempor fringilla mi. Elit a blandit faucibus est, dui interdum ut amet. Adipiscing feugiat vel at posuere in. Pellentesque volutpat vestibulum.</p>
                <a href="#" class="btn btn-warning text-white">Pelajari Lebih Lanjut</a>


            </div>
            <div class="col-3">
                <div class="col-3">
                    <div class="card card-aboutus" style="width: 200px; height: 200px">
                        <h1 class="text-center">250+</h1>
                        <p class="text-center"> Global Customer</p>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card " style="width: 200px; height: 200px">
                        <h1 class="text-center">
                            50+
                        </h1>
                        <p class="text-center">Team Member</p>
                    </div>
                </div>


            </div>
            <div class="col-3"  style="margin-top: 30px; margin-bottom:50px">
                <div class="col-3">
                    <div class="card card-aboutus" style="width: 200px; height: 200px">
                        <h1 class="text-center">156+</h1>
                        <p class="text-center">Project Complete</p>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card " style="width: 200px; height: 200px">
                        <h1 class="text-center">
                            15+
                        </h1>
                        <p class="text-center">Our Company</p>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
<!-- End Our Experioence--->

<!-- Team Member--->
<section id="teammember">
<div class="row">
    <div class="d-flex justify-content-center" style="margin-top: 100px">
        <h5 style="color: #7B7B7B"> Team Member</h5>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: 20px">
        <h1 class="hero-font text-center" style="font-weight: 700; font-size:44px; font-style:normal">
            We Have <br> <span style="color:darkorange"> Some Awesome
                </span> People
        </h1>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="row justify-content-center">
                <div class="card" style="width: 18rem;">
                    <img src="/images/aboutus/Image1.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Gustavo Herwitz</h5>
                            <p class="card-title">CEO</p>

                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img src="/images/aboutus/Image2.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Phillip Curtis</h5>
                            <p class="card-title">CFO</p>
                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img src="/images/aboutus/Image3.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Talan Torff</h5>
                            <p class="card-title">CMO</p>
                    </div>
                </div>
                <div class="card" style="width: 18rem;">
                    <img src="/images/aboutus/Image4.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Abram Vaccaro</h5>
                            <p class="card-title">CTO</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- End Team Member--->

<section id="subscribe">
    <div class="container subscribe" style="margin-top:50px; width: 1176px; height:307px; margin-bottom:50px " >
        <div class="card">
            <div class="row justify-content-center" style="margin-top: 50px">
                <p class="text-white">Subscribe Newsletters  ______</p>
                <div class="d-flex">
                    <div class="col-5">
                        <h4 class="text-white"><strong> Let's Stay in Touch </strong></h4>
                    </div>
                   <div class="col-7">
                    <div class="input-group mb-3">
                        <input type="email"
                        style="width:300px; height:60px"
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Enter your email"
                         aria-label="Recipient's username" >
                        <button style="height: 60px" class="btn btn-outline-light text-white" type="button" id="button-addon2">Subscribe Now</button>
                      </div>


                   </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="footer" style="background-color:#373737; ">
    <div class="row justify-content-center">
        <div class="col-sm-4 " style="margin-top:50px" >
            <div class="row" style="margin-left: 50px; ">
                <img style="width:193px; height:61px" src="/images/roatexwhite.png" alt="">
                <p style="margin-top: 30px" class="text-white"> Indonesia Stock Exchange Building Tower II, 19th Floor, Suite 1903, Jl. Jendral Sudirman, Kav 52-53, Lot 2 - Jakarta 12190</p>
                <div class="col">
                    <img src="/images/icon/facebook-fill.png" alt="">
                    <img style="margin-left: 10px" src="/images/icon/instagram-fill.png" alt="">
                    <img src="/images/icon/twitter-fill.png" alt="">
                    <img src="/images/icon/skype-fill.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-md-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold" style="margin-left:-200">Company</h4>
                <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">About Us</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Work</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Client</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Our Blog</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter" style="text-decoration: none;">Contact Us</a>
                  </p>

            </div>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:50px">
                <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold">Services</h4>
                <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">MLFF Account</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Get a Cassual Pass</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Installing Tag</a>
                  </p>
                  <p>
                    <a href="#" class="text-white fw-lighter fs-6" style="text-decoration: none;">Pay Toll</a>
                  </p>
            </div>
        </div>
        <div class="col-md-1 col-lg-2 col-xl-2 mx-auto mt-3" >
            <div class="row" style="margin-left: 30px; margin-top:70px; margin-bottom:100px">
                <p class="text-secondary">Discover Cantas App</p>
                <img style="margin-bottom: 10px" src="../assets/images/logo.png" alt="">
                <div class="col">
                    <img style="margin-top:10px" src="../assets/images/Googleplay.png" alt="">
                    <img style="margin-top:10px" src="../assets/images/Playstore.png" alt="">
                </div>
            </div>
        </div>
    </div>

</section>

<section id="copyright" style="background-color:#F1A501">
    <div class="container-copyright">
        <div class="row" >
            <div class="col-1" style="margin-top: 10px">
                <img src="../assets/images/icon/Group.png" alt="" style="width:25px; height:25px; margin-left:30px">

             </div>
             <div class="col-7" style="margin-top: 10px">
                <p style="margin-left:-50px" class="text-white" > Copyright 2021. All Right Reserved By PT. Roatex Indonesia Toll System</p>

             </div>
        </div>
    </div>
</section>

@endsection
