<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <!-- <link rel="stylesheet" href="bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet"href="css/lightslider.css">
    <!--Jquery-------------------->
    <script type="text/javascript" src="js/Jquery.js"></script>
    <!--lightslider.js--------------->
    <script type="text/javascript" src="js/lightslider.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;700&display=swap" rel="stylesheet">

    <style>
        .hero {
            background: #FFF7F4;
            position: absolute;
            width: 1440px;
            height: 719px;
        }
        .about-us {
            background: #FFFFFF;
            position: absolute;
            width: 1440px;
            height: 719px;
        }
        .row{

            margin-left: 0.25rem;
        }
        .col-row {
            margin-top: 1rem;
        }
        .card-title {
            margin-left: 1rem;
        }
        .card-text {
            margin-left: 1rem;
        }
        .card-link{
            margin-left: 1rem;
            margin-bottom: 1rem;
        }

    </style>
</head>
<body>
        <nav class="navbar navbar-expand-lg bg-light">
            <div class="container-fluid">
                <img src="/assets/roatex.png" alt="Roatex">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-3 mb-2 mb-lg-0 mx-auto">
                    <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="{{route('homepage')}}">Home</a>
                    </li>
                    <li class="nav-item">
              <a class="nav-link" href="{{route('aboutus-index')}}">About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('solution-index')}}">Solution</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('tech-index') }}">Technology</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('news-index')}}">News</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('career-index')}}">Career</a>
            </li>
                    <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        EN
                    </a>
                    <ul class="dropdown-menu" style="margin-left: -130px">
                        <li><a class="dropdown-item" href="#">English</a></li>
                        <li><a class="dropdown-item" href="#">Indonesia</a></li>
                    </ul>
                    </li>
                </ul>
                </div>
            </div>
        </nav>

        @yield('content')
        <!-- <script src="bootstrap.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/js/app-style-switcher.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('dist/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dist/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('dist/js/custom.js') }}"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="{{ asset('assets/libs/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.j') }}s"></script>
    <script src="{{ asset('dist/js/pages/dashboards/dashboard1.js') }}"></script>
    <script src="js/script.js" type="text/javascript"></script>
</body>
</html>
