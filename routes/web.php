<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home\HomepageController;
use App\Http\Controllers\Home\TechnologyController;
use App\Http\Controllers\Home\CareerController;
use App\Http\Controllers\Home\SolutionController;
use App\Http\Controllers\Home\NewsController;
use App\Http\Controllers\Home\AboutUsController;

use App\Http\Controllers\Home\ServicesController;


Route::get('/', [HomepageController::class, 'index'])->name('homepage');

Route::get('/technology', [TechnologyController::class, 'index'])->name('tech-index');

Route::get('/aboutus', [AboutUsController::class, 'index'])->name('aboutus-index');

Route::get('/career', [CareerController::class, 'index'])->name('career-index');

Route::get('/news', [NewsController::class, 'index'])->name('news-index');

Route::get('/solution', [SolutionController::class, 'index'])->name('solution-index');

Route::get('/services', [ServicesController::class, 'index'])->name('services-index');



